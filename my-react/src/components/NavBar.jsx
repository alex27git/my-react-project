import React from 'react';
import {Container, Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";

const NavBar = () => {
    return (
        <Navbar  bg="warning">
            <Container>
                <div className='red'>My react</div>
                <Nav>
                    <Link to='/'>Home</Link>
                    <Link to='/auth'>Auth</Link>
                </Nav>
            </Container>
        </Navbar>
    );
};

export default NavBar;