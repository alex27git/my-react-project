import React from 'react';
import {ListGroup} from "react-bootstrap";

const TypeBar = () => {
    const food = [
        {id:1, name: 'pizza'},
        {id:2, name: 'beverages'},
        {id:3, name: 'cheese'},
        {id:4, name: 'milk'},
        {id:5, name: 'fruit'},
    ]
    return (
        <ListGroup className="mt-4">
            {food.map(item =>
                <ListGroup.Item key={item.id}>
                    {item.name}
                </ListGroup.Item>
            )}
        </ListGroup>
    );
};

export default TypeBar;