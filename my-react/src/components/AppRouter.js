import React from 'react';
import {Route, Routes} from "react-router-dom";
import Home from "../pages/Home";
import Auth from "../pages/Auth";
import ProductPage from "../pages/ProductPage";

const AppRouter = () => {
    return (
        <Routes>
            <Route path={'/'} element={<Home/>}/>
            <Route path={'/auth'} element={<Auth/>}/>
            <Route path={'/:id'} element={<ProductPage/>}/>
        </Routes>
    );
};

export default AppRouter;