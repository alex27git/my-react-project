import src1 from "../assets/1.jpg";
import src2 from "../assets/2.jpg";
const initialState = {
    cards: [
        {
            id:1,
            title: 'pizza five cheese',
            price: '550',
            desc: 'pizza five cheese is nice pizza',
            img: src1,
        },
        {
            id:2,
            title: 'pizza five cheese',
            price: '550',
            desc: 'pizza five cheese is nice pizza',
            img: src2,
        },
        {
            id:3,
            title: 'pizza five cheese',
            price: '550',
            desc: 'pizza five cheese is nice pizza',
            img: src1,
        },
        {
            id:4,
            title: 'pizza five cheese',
            price: '550',
            desc: 'pizza five cheese is nice pizza',
            img: src2,
        },
    ]
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "CREATE_PRODUCT":
            return {...state, cards: [...state.cards, action.data]}
        case "REMOVE_PRODUCT":
            return {...state, cards: state.cards.filter(product => product.id !== action.id)}
        default:
            return state
    }
}