import React from 'react';
import {Card, Col, Container, Row} from "react-bootstrap";
import NavBar from "../components/NavBar";
import TypeBar from "../components/TypeBar";
import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";

const ProductPage = () => {
    const params = useParams()
    const id = params.id - 1
    const product = useSelector(state => state.cards[id])
    return (
        <>
            <NavBar/>
            <Container>
                <Row>
                    <Col md={3}>
                        <TypeBar/>
                    </Col>
                    <Col md={9}>
                        <Row className='mt-4'>
                            <Col md={6}>
                                <Card>
                                    <Card.Img height={300} variant='top' src={product.img}></Card.Img>
                                </Card>
                            </Col>
                            <Col md={6}>
                                <div>{product.price}&#8381;</div>
                                <h1>{product.title}</h1>
                                <div>{product.desc}</div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>

        </>

    );
};

export default ProductPage;