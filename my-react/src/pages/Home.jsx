import React from 'react';
import {Col, Container, Row} from "react-bootstrap";
import TypeBar from "../components/TypeBar";
import ProductList from "../components/ProductList";
import NavBar from "../components/NavBar";

const Home = () => {
    return (
        <>
            <NavBar/>
            <Container>
                <Row>
                    <Col md={3}>
                        <TypeBar/>
                    </Col>
                    <Col md={9}>
                        <ProductList/>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default Home;